<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::updateOrCreate([
            'product_name' => 'Product Coba',
            'description'=> 'testing product',
            'stock' => 10,
            'buying_price' => 10000,
            'selling_price' => 11000
        ], [
            'product_name' => 'Product Coba',
            'description'=> 'testing product',
            'stock' => 10,
            'buying_price' => 10000,
            'selling_price' => 11000
        ]);
    }
}
