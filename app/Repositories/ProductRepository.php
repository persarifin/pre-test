<?php

namespace App\Repositories;

use App\Models\Product;
use App\Http\Criterias\SearchCriteria;
use App\Http\Presenters\DataPresenter;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
	public function __construct()
	{
		parent::__construct(Product::class);
	}

	public function browse($request)
	{
		try{
			$this->query = $this->getModel();
			$this->applyCriteria(new SearchCriteria($request));

			return $this->renderCollection($request);
		}catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
		}
	}

	public function show($id, $request)
	{
		try{
			$this->query = $this->getModel()->where(['id' => $id]);
			$this->applyCriteria(new SearchCriteria($request));

			return $this->render($request);
		}catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
		}
	}

	public function store($request)
	{
		try {
			$payload = $request->all();
			$product = Product::create($payload);

			return $this->show($product->id, $request);
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
		}
	}

	public function update($id, $request)
	{
		try {
			$payload = $request->all();
			Product::findOrFail($id)->update($payload);

			return $this->show($id, $request);
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
		}
	}

	public function destroy($id)
	{
		try {
			Product::findOrFail($id)->delete();

			return response()->json([
				'success' => true,
				'message' => 'data has been deleted'
			], 200);
		} catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
		}
	}
}
