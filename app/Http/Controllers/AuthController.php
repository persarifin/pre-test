<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        try {
            $user = User::whereRaw('lower(email) = ?', strtolower($request->email))->first();

            if ($user && \Hash::check($request->password, $user->password)) {
                $data = [
                    'user' => $user,
                    'token' => $user->createToken($user->id . '-'. $user->name)->accessToken,
                ];

                return response()->json([
                    'success' => true,
                    'data' => $data,
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid email or password.',
                ], 403);
            }
        } catch (\Exception $e) {
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			], 400);
        }
    }
}
